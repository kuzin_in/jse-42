package ru.kuzin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.kuzin.tm.api.repository.IUserRepository;
import ru.kuzin.tm.api.service.IConnectionService;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.dto.model.UserDTO;
import ru.kuzin.tm.exception.entity.EntityNotFoundException;
import ru.kuzin.tm.marker.UnitCategory;
import ru.kuzin.tm.service.ConnectionService;
import ru.kuzin.tm.service.PropertyService;

import java.sql.Connection;

import static ru.kuzin.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private final IUserRepository repository = new UserRepository();

    private final IUserRepository emptyRepository = new UserRepository();

    @Before
    public void before() throws Exception {
        repository.add(USER_TEST);
    }

    @After
    public void after() throws Exception {
        repository.removeAll(USER_LIST);
    }

    @Test(expected = NullPointerException.class)
    public void add() throws Exception {
        Assert.assertNull(repository.add(NULL_USER));
        Assert.assertNotNull(repository.add(ADMIN_TEST));
        @Nullable final UserDTO user = repository.findOneById(ADMIN_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST, user);
    }

    @Test
    public void addMany() throws Exception {
        Assert.assertNotNull(repository.add(USER_LIST_ADDED));
        for (final UserDTO user : USER_LIST_ADDED)
            Assert.assertEquals(user, repository.findOneById(user.getId()));
    }

    @Test
    public void set() throws Exception {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_LIST_ADDED);
        emptyRepository.set(USER_LIST);
        Assert.assertEquals(USER_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAll() throws Exception {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_LIST_ADDED);
        Assert.assertEquals(USER_LIST_ADDED, emptyRepository.findAll());
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(repository.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(repository.existsById(USER_TEST.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertNull(repository.findOneById(null));
        Assert.assertNull(repository.findOneById(NON_EXISTING_USER_ID));
        @Nullable final UserDTO user = repository.findOneById(USER_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST, user);
    }

    @Test
    public void clear() throws Exception {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_LIST_ADDED);
        emptyRepository.clear();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test(expected = EntityNotFoundException.class)
    public void remove() throws Exception {
        Assert.assertNull(repository.remove(null));
        @Nullable final UserDTO createdUser = repository.add(ADMIN_TEST);
        @Nullable final UserDTO removedUser = repository.remove(createdUser);
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_TEST, removedUser);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeById() throws Exception {
        Assert.assertNull(repository.removeById(null));
        Assert.assertNull(repository.removeById(NON_EXISTING_USER_ID));
        @Nullable final UserDTO createdUser = repository.add(ADMIN_TEST);
        @Nullable final UserDTO removedUser = repository.removeById(ADMIN_TEST.getId());
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_TEST, removedUser);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeByIndex() throws Exception {
        Assert.assertNull(repository.removeByIndex(null));
        @Nullable final UserDTO createdUser = repository.add(ADMIN_TEST);
        final int index = repository.findAll().indexOf(createdUser);
        @Nullable final UserDTO removedUser = repository.removeByIndex(index);
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_TEST, removedUser);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId()));
    }

    @Test
    public void getSize() throws Exception {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        Assert.assertEquals(0, emptyRepository.getSize());
        emptyRepository.add(ADMIN_TEST);
        Assert.assertEquals(1, emptyRepository.getSize());
    }

    @Test
    public void removeAll() throws Exception {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_LIST);
        emptyRepository.removeAll(USER_LIST);
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test(expected = NullPointerException.class)
    public void findByLogin() throws Exception {
        Assert.assertNull(repository.findByLogin(null));
        @Nullable final UserDTO user = repository.findByLogin(USER_TEST.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST, user);
    }

    @Test(expected = NullPointerException.class)
    public void findByEmail() throws Exception {
        Assert.assertNull(repository.findByEmail(null));
        @Nullable final UserDTO user = repository.findByEmail(USER_TEST.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST, user);
    }

    @Test(expected = NullPointerException.class)
    public void isLoginExists() throws Exception {
        Assert.assertFalse(repository.isLoginExist(null));
        Assert.assertTrue(repository.isLoginExist(USER_TEST.getLogin()));
    }

    @Test(expected = NullPointerException.class)
    public void isEmailExists() throws Exception {
        Assert.assertFalse(repository.isEmailExist(null));
        Assert.assertTrue(repository.isEmailExist(USER_TEST.getEmail()));
    }

}