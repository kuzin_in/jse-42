package ru.kuzin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.kuzin.tm.api.repository.IProjectRepository;
import ru.kuzin.tm.api.service.IConnectionService;
import ru.kuzin.tm.api.service.IProjectService;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.comparator.NameComparator;
import ru.kuzin.tm.dto.model.ProjectDTO;
import ru.kuzin.tm.enumerated.Sort;
import ru.kuzin.tm.enumerated.Status;
import ru.kuzin.tm.exception.entity.EntityNotFoundException;
import ru.kuzin.tm.exception.entity.ProjectNotFoundException;
import ru.kuzin.tm.exception.field.*;
import ru.kuzin.tm.marker.UnitCategory;
import ru.kuzin.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.util.Comparator;
import java.util.stream.Collectors;

import static ru.kuzin.tm.constant.ProjectTestData.*;
import static ru.kuzin.tm.constant.UserTestData.ADMIN_TEST;
import static ru.kuzin.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    private static final IPropertyService propertyService = new PropertyService();

    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private final IProjectRepository repository = new ProjectRepository();

    @NotNull
    private final IProjectService service = new ProjectService(repository, connectionService);

    @NotNull
    private final IProjectService emptyService = new ProjectService(repository, connectionService);

    @Before
    public void before() throws Exception {
        repository.add(USER_PROJECT1);
        repository.add(USER_PROJECT2);
    }

    @After
    public void after() throws Exception {
        repository.removeAll(PROJECT_LIST);
    }

    @Test
    public void add() throws Exception {
        Assert.assertNull(service.add(NULL_PROJECT));
        Assert.assertNotNull(service.add(ADMIN_PROJECT1));
        @Nullable final ProjectDTO project = service.findOneById(ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1, project);
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertNull(service.add(ADMIN_TEST.getId(), NULL_PROJECT));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add(null, ADMIN_PROJECT1);
        });
        Assert.assertNotNull(service.add(ADMIN_TEST.getId(), ADMIN_PROJECT1));
        @Nullable final ProjectDTO project = service.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1, project);
    }

    @Test
    public void addMany() throws Exception {
        Assert.assertNotNull(service.add(ADMIN_PROJECT_LIST));
        for (final ProjectDTO project : ADMIN_PROJECT_LIST)
            Assert.assertEquals(project, service.findOneById(project.getId()));
    }

    @Test
    public void set() throws Exception {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        @NotNull final IProjectService emptyService = new ProjectService(emptyRepository, connectionService);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_PROJECT_LIST);
        emptyService.set(ADMIN_PROJECT_LIST);
        Assert.assertEquals(ADMIN_PROJECT_LIST, emptyService.findAll());
    }

    @Test
    public void findAll() throws Exception {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        @NotNull final IProjectService emptyService = new ProjectService(emptyRepository, connectionService);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_PROJECT_LIST);
        Assert.assertEquals(USER_PROJECT_LIST, emptyService.findAll());
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll("");
        });
        Assert.assertEquals(USER_PROJECT_LIST, service.findAll(USER_TEST.getId()));
    }

    @Test
    public void findAllComparatorByUserId() throws Exception {
        @Nullable Comparator comparator = null;
        Assert.assertEquals(USER_PROJECT_LIST, service.findAll(USER_TEST.getId(), comparator));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Comparator comparatorInner = null;
            service.findAll("", comparatorInner);
        });
        comparator = NameComparator.INSTANCE;
        Assert.assertEquals(USER_PROJECT_LIST.stream().sorted(comparator).collect(Collectors.toList()), service.findAll(USER_TEST.getId(), comparator));
    }

    @Test
    public void findAllSortByUserId() throws Exception {
        @Nullable Sort sort = null;
        Assert.assertEquals(USER_PROJECT_LIST, service.findAll(USER_TEST.getId(), sort));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Sort sortInner = null;
            service.findAll("", sortInner);
        });
        sort = Sort.BY_NAME;
        Assert.assertEquals(USER_PROJECT_LIST.stream().sorted(sort.getComparator()).collect(Collectors.toList()), service.findAll(USER_TEST.getId(), sort.getComparator()));
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(service.existsById(""));
        Assert.assertFalse(service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(service.existsById(USER_PROJECT1.getId()));
    }

    @Test(expected = IdEmptyException.class)
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, NON_EXISTING_PROJECT_ID);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", NON_EXISTING_PROJECT_ID);
        });
        Assert.assertFalse(service.existsById(USER_TEST.getId(), null));
        Assert.assertFalse(service.existsById(USER_TEST.getId(), ""));
        Assert.assertFalse(service.existsById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(service.existsById(USER_TEST.getId(), USER_PROJECT1.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertNull(service.findOneById(NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = service.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1, project);
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USER_TEST.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USER_TEST.getId(), "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, USER_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", USER_PROJECT1.getId());
        });
        Assert.assertNull(service.findOneById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = service.findOneById(USER_TEST.getId(), USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1, project);
    }

    @Test
    public void findOneByIndexByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex("", null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(USER_TEST.getId(), null);
        });
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            service.findOneByIndex(USER_TEST.getId(), -1);
        });
        final int index = service.findAll().indexOf(USER_PROJECT1);
        @Nullable final ProjectDTO project = service.findOneByIndex(USER_TEST.getId(), index);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1, project);
    }

    @Test
    public void clear() throws Exception {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        @NotNull final IProjectService emptyService = new ProjectService(emptyRepository, connectionService);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_PROJECT_LIST);
        emptyService.clear();
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void clearByUserId() throws Exception {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        @NotNull final IProjectService emptyService = new ProjectService(emptyRepository, connectionService);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.clear(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.clear("");
        });
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_PROJECT1);
        emptyService.add(USER_PROJECT2);
        emptyService.clear(USER_TEST.getId());
        Assert.assertEquals(0, emptyService.getSize(USER_TEST.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void remove() throws Exception {
        Assert.assertNull(service.remove(null));
        @Nullable final ProjectDTO createdProject = service.add(ADMIN_PROJECT1);
        @Nullable final ProjectDTO removedProject = service.remove(createdProject);
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(service.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove("", null);
        });
        Assert.assertNull(service.remove(ADMIN_TEST.getId(), null));
        @Nullable final ProjectDTO createdProject = service.add(ADMIN_PROJECT1);
        @Nullable final ProjectDTO removedProject = service.remove(ADMIN_TEST.getId(), createdProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT1.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById("");
        });
        Assert.assertNull(service.removeById(NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO createdProject = service.add(ADMIN_PROJECT1);
        @Nullable final ProjectDTO removedProject = service.removeById(ADMIN_PROJECT1.getId());
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(service.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USER_TEST.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USER_TEST.getId(), "");
        });
        Assert.assertNull(service.removeById(ADMIN_TEST.getId(), NON_EXISTING_PROJECT_ID));
        Assert.assertNull(service.removeById(ADMIN_TEST.getId(), USER_PROJECT1.getId()));
        @Nullable final ProjectDTO createdProject = service.add(ADMIN_PROJECT1);
        @Nullable final ProjectDTO removedProject = service.removeById(ADMIN_TEST.getId(), createdProject.getId());
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT1.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeByIndex() throws Exception {
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(-1);
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.removeByIndex(service.getSize());
        });
        @Nullable final ProjectDTO createdProject = service.add(ADMIN_PROJECT1);
        final int index = service.findAll().indexOf(createdProject);
        @Nullable final ProjectDTO removedProject = service.removeByIndex(index);
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(service.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeByIndexByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByIndex(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByIndex("", null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(USER_TEST.getId(), null);
        });
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            service.removeByIndex(USER_TEST.getId(), -1);
        });
        @Nullable final ProjectDTO createdProject = service.add(ADMIN_PROJECT1);
        final int index = service.findAll(ADMIN_TEST.getId()).indexOf(createdProject);
        @Nullable final ProjectDTO removedProject = service.removeByIndex(ADMIN_TEST.getId(), index);
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT1.getId()));
    }

    @Test
    public void getSize() throws Exception {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        @NotNull final IProjectService emptyService = new ProjectService(emptyRepository, connectionService);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        Assert.assertEquals(0, emptyService.getSize());
        emptyService.add(ADMIN_PROJECT1);
        Assert.assertEquals(1, emptyService.getSize());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        @NotNull final IProjectService emptyService = new ProjectService(emptyRepository, connectionService);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.getSize(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.getSize("");
        });
        Assert.assertTrue(emptyService.findAll().isEmpty());
        Assert.assertEquals(0, emptyService.getSize(ADMIN_TEST.getId()));
        emptyService.add(ADMIN_PROJECT1);
        emptyService.add(USER_PROJECT1);
        Assert.assertEquals(1, emptyService.getSize(ADMIN_TEST.getId()));
    }

    @Test
    public void removeAll() throws Exception {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        @NotNull final IProjectService emptyService = new ProjectService(emptyRepository, connectionService);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(PROJECT_LIST);
        emptyService.removeAll(PROJECT_LIST);
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void create() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, ADMIN_PROJECT1.getName());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", ADMIN_PROJECT1.getName());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ADMIN_TEST.getId(), null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ADMIN_TEST.getId(), "");
        });
        @NotNull final ProjectDTO project = service.create(ADMIN_TEST.getId(), ADMIN_PROJECT1.getName());
        Assert.assertEquals(project, service.findOneById(ADMIN_TEST.getId(), project.getId()));
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_TEST.getId(), project.getUserId());
    }

    @Test
    public void createWithDescription() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ADMIN_TEST.getId(), null, ADMIN_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ADMIN_TEST.getId(), "", ADMIN_PROJECT1.getDescription());
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create(ADMIN_TEST.getId(), ADMIN_PROJECT1.getName(), null);
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create(ADMIN_TEST.getId(), ADMIN_PROJECT1.getName(), "");
        });
        @NotNull final ProjectDTO project = service.create(ADMIN_TEST.getId(), ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription());
        Assert.assertEquals(project, service.findOneById(ADMIN_TEST.getId(), project.getId()));
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(ADMIN_TEST.getId(), project.getUserId());
    }

    @Test
    public void updateById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById(null, USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById("", USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(USER_TEST.getId(), null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(USER_TEST.getId(), "", USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(USER_TEST.getId(), USER_PROJECT1.getId(), null, USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(USER_TEST.getId(), USER_PROJECT1.getId(), "", USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.updateById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        @NotNull final String name = USER_PROJECT1.getName() + NON_EXISTING_PROJECT_ID;
        @NotNull final String description = USER_PROJECT1.getDescription() + NON_EXISTING_PROJECT_ID;
        service.updateById(USER_TEST.getId(), USER_PROJECT1.getId(), name, description);
        Assert.assertEquals(name, USER_PROJECT1.getName());
        Assert.assertEquals(description, USER_PROJECT1.getDescription());
    }

    @Test
    public void updateByIndex() throws Exception {
        @Nullable final ProjectDTO project = service.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        final int index = service.findAll().indexOf(project);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateByIndex(null, index, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateByIndex("", index, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.updateByIndex(USER_TEST.getId(), null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.updateByIndex(USER_TEST.getId(), -1, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.updateByIndex(USER_TEST.getId(), service.getSize(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateByIndex(USER_TEST.getId(), index, null, USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateByIndex(USER_TEST.getId(), index, "", USER_PROJECT1.getDescription());
        });
        @NotNull final String name = USER_PROJECT1.getName() + NON_EXISTING_PROJECT_ID;
        @NotNull final String description = USER_PROJECT1.getDescription() + NON_EXISTING_PROJECT_ID;
        service.updateByIndex(USER_TEST.getId(), index, name, description);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test
    public void changeProjectStatusById() throws Exception {
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusById(null, USER_PROJECT1.getId(), status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusById("", USER_PROJECT1.getId(), status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeProjectStatusById(USER_TEST.getId(), null, status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeProjectStatusById(USER_TEST.getId(), "", status);
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.changeProjectStatusById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID, status);
        });
        service.changeProjectStatusById(USER_TEST.getId(), USER_PROJECT1.getId(), status);
        Assert.assertNotNull(USER_PROJECT1);
        Assert.assertEquals(status, USER_PROJECT1.getStatus());
    }

    @Test
    public void changeProjectStatusByIndex() throws Exception {
        @NotNull final Status status = Status.COMPLETED;
        @Nullable final ProjectDTO project = service.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        final int index = service.findAll().indexOf(project);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusByIndex(null, index, status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusByIndex("", index, status);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeProjectStatusByIndex(USER_TEST.getId(), null, status);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeProjectStatusByIndex(USER_TEST.getId(), -1, status);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeProjectStatusByIndex(USER_TEST.getId(), service.getSize(), status);
        });
        service.changeProjectStatusByIndex(USER_TEST.getId(), index, status);
        Assert.assertEquals(status, project.getStatus());
    }

}