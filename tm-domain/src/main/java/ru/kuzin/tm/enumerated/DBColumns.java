package ru.kuzin.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum DBColumns {

    ID_COLUMN("id"),
    CREATED_COLUMN("created"),
    USER_ID_COLUMN("user_id"),
    NAME_COLUMN("name"),
    DESCRIPTION_COLUMN("description"),
    STATUS_COLUMN("status"),
    PROJECT_ID_COLUMN("project_id"),
    ROLE_COLUMN("role"),
    FIRST_NAME_COLUMN("first_name"),
    LAST_NAME_COLUMN("last_name"),
    MIDDLE_NAME_COLUMN("middle_name"),
    EMAIL_COLUMN("email"),
    LOGIN_COLUMN("login"),
    PASSWORD_COLUMN("password_hash"),
    SESSION_DATE_COLUMN("session_date"),
    LOCKED_COLUMN("locked");

    @NotNull
    private final String columnName;

    DBColumns(@NotNull final String columnName) {
        this.columnName = columnName;
    }

    @NotNull
    public String getColumnName() {
        return columnName;
    }

}